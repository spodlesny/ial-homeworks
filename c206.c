
/* c206.c **********************************************************}
{* Téma: Dvousměrně vázaný lineární seznam
**
**                   Návrh a referenční implementace: Bohuslav Křena, říjen 2001
**                            Přepracované do jazyka C: Martin Tuček, říjen 2004
**                                            Úpravy: Bohuslav Křena, říjen 2016
**
** Implementujte abstraktní datový typ dvousměrně vázaný lineární seznam.
** Užitečným obsahem prvku seznamu je hodnota typu int.
** Seznam bude jako datová abstrakce reprezentován proměnnou
** typu tDLList (DL znamená Double-Linked a slouží pro odlišení
** jmen konstant, typů a funkcí od jmen u jednosměrně vázaného lineárního
** seznamu). Definici konstant a typů naleznete v hlavičkovém souboru c206.h.
**
** Vaším úkolem je implementovat následující operace, které spolu
** s výše uvedenou datovou částí abstrakce tvoří abstraktní datový typ
** obousměrně vázaný lineární seznam:
**
**      DLInitList ...... inicializace seznamu před prvním použitím,
**      DLDisposeList ... zrušení všech prvků seznamu,
**      DLInsertFirst ... vložení prvku na začátek seznamu,
**      DLInsertLast .... vložení prvku na konec seznamu,
**      DLFirst ......... nastavení aktivity na první prvek,
**      DLLast .......... nastavení aktivity na poslední prvek,
**      DLCopyFirst ..... vrací hodnotu prvního prvku,
**      DLCopyLast ...... vrací hodnotu posledního prvku,
**      DLDeleteFirst ... zruší první prvek seznamu,
**      DLDeleteLast .... zruší poslední prvek seznamu,
**      DLPostDelete .... ruší prvek za aktivním prvkem,
**      DLPreDelete ..... ruší prvek před aktivním prvkem,
**      DLPostInsert .... vloží nový prvek za aktivní prvek seznamu,
**      DLPreInsert ..... vloží nový prvek před aktivní prvek seznamu,
**      DLCopy .......... vrací hodnotu aktivního prvku,
**      DLActualize ..... přepíše obsah aktivního prvku novou hodnotou,
**      DLSucc .......... posune aktivitu na další prvek seznamu,
**      DLPred .......... posune aktivitu na předchozí prvek seznamu,
**      DLActive ........ zjišťuje aktivitu seznamu.
**
** Při implementaci jednotlivých funkcí nevolejte žádnou z funkcí
** implementovaných v rámci tohoto příkladu, není-li u funkce
** explicitně uvedeno něco jiného.
**
** Nemusíte ošetřovat situaci, kdy místo legálního ukazatele na seznam
** předá někdo jako parametr hodnotu NULL.
**
** Svou implementaci vhodně komentujte!
**
** Terminologická poznámka: Jazyk C nepoužívá pojem procedura.
** Proto zde používáme pojem funkce i pro operace, které by byly
** v algoritmickém jazyce Pascalovského typu implemenovány jako
** procedury (v jazyce C procedurám odpovídají funkce vracející typ void).
**/

//Author: xpodle01 - Šimon Podlesný

#include "c206.h"

int solved;
int errflg;

void DLError() {
/*
** Vytiskne upozornění na to, že došlo k chybě.
** Tato funkce bude volána z některých dále implementovaných operací.
**/
    printf ("*ERROR* The program has performed an illegal operation.\n");
    errflg = TRUE;             /* globální proměnná -- příznak ošetření chyby */
    return;
}

void DLInitList (tDLList *L) {
/*
** Provede inicializaci seznamu L před jeho prvním použitím (tzn. žádná
** z následujících funkcí nebude volána nad neinicializovaným seznamem).
** Tato inicializace se nikdy nebude provádět nad již inicializovaným
** seznamem, a proto tuto možnost neošetřujte. Vždy předpokládejte,
** že neinicializované proměnné mají nedefinovanou hodnotu.
**/

//inicializacia double linked list
 L->First = NULL;
 L->Last = NULL;
 L->Act = NULL;
}

void DLDisposeList (tDLList *L) {
/*
** Zruší všechny prvky seznamu L a uvede seznam do stavu, v jakém
** se nacházel po inicializaci. Rušené prvky seznamu budou korektně
** uvolněny voláním operace free.
**/

 	while (L->First != NULL){
		tDLElemPtr second_elem = L->First->rptr;
		free(L->First);
		L->First = second_elem;
	}

	//nastavim Act aj Last na NULL aby neukazovali na nemalocnutu pamäť
	L->Act = NULL;
	L->Last = NULL;

}

void DLInsertFirst (tDLList *L, int val) {
/*
** Vloží nový prvek na začátek seznamu L.
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/

	tDLElemPtr new_elem = malloc(sizeof(struct tDLElem));

	if (new_elem == NULL) {
		DLError();
		return;
	}

	new_elem->data = val;
	new_elem->lptr = NULL;
	new_elem->rptr = L->First;

	if (L->First != NULL) {
		L->First->lptr = new_elem;
	}
	else{
		L->Last = new_elem;
	}

	L->First = new_elem;
}

void DLInsertLast(tDLList *L, int val) { //ToDo: neotestovana!
/*
** Vloží nový prvek na konec seznamu L (symetrická operace k DLInsertFirst).
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/

	tDLElemPtr new_elem = malloc(sizeof(struct tDLElem));

	if (new_elem == NULL) {
		DLError();
		return;
	}

	new_elem->data = val;
	new_elem->lptr = L->Last;
	new_elem->rptr = NULL;
	if (L->First != NULL) { //Last/First?
		L->Last->rptr = new_elem;
	}
	else{
		L->First = new_elem;
	}
	L->Last = new_elem;

}

void DLFirst (tDLList *L) {
/*
** Nastaví aktivitu na první prvek seznamu L.
** Funkci implementujte jako jediný příkaz (nepočítáme-li return),
** aniž byste testovali, zda je seznam L prázdný.
**/


 L->Act = L->First;
}

void DLLast (tDLList *L) {
/*
** Nastaví aktivitu na poslední prvek seznamu L.
** Funkci implementujte jako jediný příkaz (nepočítáme-li return),
** aniž byste testovali, zda je seznam L prázdný.
**/


 L->Act = L->Last;
}

void DLCopyFirst (tDLList *L, int *val) {
/*
** Prostřednictvím parametru val vrátí hodnotu prvního prvku seznamu L.
** Pokud je seznam L prázdný, volá funkci DLError().
**/

	if (L->First == NULL){
		DLError();
		return;
	}

	*val = L->First->data;
}

void DLCopyLast (tDLList *L, int *val) {
/*
** Prostřednictvím parametru val vrátí hodnotu posledního prvku seznamu L.
** Pokud je seznam L prázdný, volá funkci DLError().
**/


 	if (L->Last == NULL){
		DLError();
		return;
	}

	*val = L->Last->data;
}

void DLDeleteFirst (tDLList *L) {
/*
** Zruší první prvek seznamu L. Pokud byl první prvek aktivní, aktivita
** se ztrácí. Pokud byl seznam L prázdný, nic se neděje.
**/


 	if (L->First == NULL){
		return;
	}
	if ( L->Act == L->First){
		L->Act = NULL;
	}
	if (L->First == L->Last){
		L->Last = NULL;
	}

	//vymazem prvy zaznam a namerujem First na druhy zaznam a pravy ukazatel druheho zaznamu presmerujem na First
	tDLElemPtr second_elem = L->First->rptr;
	free(L->First);
	L->First = second_elem;

	if (L->First != NULL) {
		L->First->lptr = NULL;
	}
}

void DLDeleteLast (tDLList *L) {
/*
** Zruší poslední prvek seznamu L. Pokud byl poslední prvek aktivní,
** aktivita seznamu se ztrácí. Pokud byl seznam L prázdný, nic se neděje.
**/


  	if (L->Last == NULL){
		return;
	}
	if ( L->Act == L->Last){
		L->Act = NULL;
	}
	if (L->First == L->Last){
		L->First = NULL;
	}

	tDLElemPtr second_elem = L->Last->lptr;
	free(L->Last);
	L->Last = second_elem;

	if (L->Last != NULL) {
		L->Last->rptr = NULL;
	}

}

void DLPostDelete (tDLList *L) {
/*
** Zruší prvek seznamu L za aktivním prvkem.
** Pokud je seznam L neaktivní nebo pokud je aktivní prvek
** posledním prvkem seznamu, nic se neděje.
**/


	if (L->Act == NULL || L->Act == L->Last) {
		return;
	}

	tDLElemPtr second_elem = L->Act->rptr;
	tDLElemPtr third_elem = second_elem->rptr;

	if (third_elem != NULL){
		third_elem->lptr = L->Act;
	}
	else{
		L->Last = L->Act; //mozno treba!
	}
	L->Act->rptr = third_elem;
	free(second_elem);

}

void DLPreDelete (tDLList *L) {
/*
** Zruší prvek před aktivním prvkem seznamu L .
** Pokud je seznam L neaktivní nebo pokud je aktivní prvek
** prvním prvkem seznamu, nic se neděje.
**/

	if (L->Act == NULL) {
		return;
	}
	if (L->Act == L->First) {
		return;
	}

	tDLElemPtr second_elem = L->Act->lptr;
	tDLElemPtr third_elem = second_elem->lptr;

	if (third_elem != NULL){
		third_elem->rptr = L->Act;
	}
	else{
		L->First = L->Act;
	}
	L->Act->lptr = third_elem;
	free(second_elem);

}

void DLPostInsert (tDLList *L, int val) {
/*
** Vloží prvek za aktivní prvek seznamu L.
** Pokud nebyl seznam L aktivní, nic se neděje.
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/

 if (L->Act == NULL) {
 	return;
 }

 tDLElemPtr new_elem = malloc(sizeof(struct tDLElem));
 tDLElemPtr post_elem = L->Act->rptr;

 if (new_elem == NULL) {
 	DLError();
 	return;
 }

 new_elem->lptr = L->Act;
 new_elem->rptr = post_elem;
 new_elem->data = val;

 if (post_elem != NULL) {
 	post_elem->lptr = new_elem;
 }
 if (L->Act == L->Last) {
 	L->Last = new_elem;
 }

 L->Act->rptr = new_elem;
}

void DLPreInsert (tDLList *L, int val) {
/*
** Vloží prvek před aktivní prvek seznamu L.
** Pokud nebyl seznam L aktivní, nic se neděje.
** V případě, že není dostatek paměti pro nový prvek při operaci malloc,
** volá funkci DLError().
**/


 if (L->Act == NULL) {
 	return;
 }

 tDLElemPtr new_elem = malloc(sizeof(struct tDLElem));
 tDLElemPtr pre_elem = L->Act->lptr;

 if (new_elem == NULL) {
 	DLError();
 	return;
 }

 new_elem->lptr = pre_elem;
 new_elem->rptr = L->Act;
 new_elem->data = val;

 if (pre_elem != NULL) {
 	pre_elem->rptr = new_elem;
 }
 if (L->Act == L->First) {
	L->First = new_elem;
 }

 L->Act->lptr = new_elem;
}

void DLCopy (tDLList *L, int *val) {
/*
** Prostřednictvím parametru val vrátí hodnotu aktivního prvku seznamu L.
** Pokud seznam L není aktivní, volá funkci DLError ().
**/



 if (L->Act == NULL) {
 	DLError();
 	return;
 }
 *val = L->Act->data;
}

void DLActualize (tDLList *L, int val) {
/*
** Přepíše obsah aktivního prvku seznamu L.
** Pokud seznam L není aktivní, nedělá nic.
**/


 if (L->Act == NULL) {
 	return;
 }
 L->Act->data = val;
}

void DLSucc (tDLList *L) {
/*
** Posune aktivitu na následující prvek seznamu L.
** Není-li seznam aktivní, nedělá nic.
** Všimněte si, že při aktivitě na posledním prvku se seznam stane neaktivním.
**/


 if (L->Act == NULL) {
 	return;
 }
 if (L->Act == L->Last) {
 	L->Act = NULL;
 	return;
 }

 tDLElemPtr second_elem = L->Act->rptr;
 L->Act = second_elem;

}


void DLPred (tDLList *L) {
/*
** Posune aktivitu na předchozí prvek seznamu L.
** Není-li seznam aktivní, nedělá nic.
** Všimněte si, že při aktivitě na prvním prvku se seznam stane neaktivním.
**/

 if (L->Act == NULL) {
 	return;
 }
 if (L->Act == L->First) {
 	L->Act = NULL;
 	return;
 }

 tDLElemPtr second_elem = L->Act->lptr;
 L->Act = second_elem;

}

int DLActive (tDLList *L) {
/*
** Je-li seznam L aktivní, vrací nenulovou hodnotu, jinak vrací 0.
** Funkci je vhodné implementovat jedním příkazem return.
**/


 return L->Act != NULL;
}

/* Konec c206.c*/
